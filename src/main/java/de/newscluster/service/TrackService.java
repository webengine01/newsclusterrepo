package de.newscluster.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import de.newscluster.model.Topic;
import de.newscluster.model.Track;
import de.newscluster.service.hibernate.SessionManager;

public class TrackService extends SessionManager {

	private static TrackService instance;

	private TrackService() {
		// Empty private singleton constructor
	}

	public static TrackService getInstance() {
		if (instance == null) {
			instance = new TrackService();
		}
		return instance;
	}

	public void addRecord(Object o) {
		session.save(o);
	}

//	private boolean isPresent(List<?> l) {
//		if (l != null) {
//			if (l.size() > 0) {
//				return true;
//			}
//		}
//		return false;
//	}

	public void insert(Track track) {
		this.openTransaction();
		addRecord(track);
		this.closeTransaction();
	}
	
	public void deleteTrackByUserAndTopic(int topicID, int userID){
		openTransaction();
		//Query query = session.createQuery("From Track");
		Query query = session.createSQLQuery("delete from track where top_id = " + +topicID + " and usr_id = " + userID );
		query.executeUpdate();
		
//		List<Track> trackRes = query.list();
//		for (Track track : trackRes) {
//			if (track.getTopicID().getTopicID() == topicID && track.getUsrID().getUsrID() == userID) {
//				System.out.println("FOUND MATCH");
//				delete(track);
//			}
//		}
	closeTransaction();
	}
	
	public void delete(Track track) {
		this.openTransaction();
		session.delete(track);
		this.closeTransaction();
	}
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<Topic> retriveByUser(int userID){
		ArrayList<Topic> topList = new ArrayList<Topic>();
		String baseQuery = "From Track where usrID=" + userID;
		this.openTransaction();
		Query query = session.createQuery(baseQuery);
		List<Track> trackRes = query.list();
		this.closeTransaction();
		for (Track track : trackRes) {
			topList.add(track.getTopicID());
		}
		return topList;
	}

}
