package de.newscluster.service.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import de.newscluster.service.ArticleService;
public class CSVReader {
	
	private File file;
	
	public CSVReader(File file) {
		this.file = file;
	}
	
	public void insertArticleFile() throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = null;
		while ((line = br.readLine()) != null) {
			String[] split = line.split(";");
			ArticleService.getInstance().insert(split[0], split[1], split[2], split[3], split[4], Integer.parseInt(split[5]), split[6], split[7], split[9]);
		}
		br.close();
	}

}
