package de.newscluster.service.utils;

import java.io.File;
import java.io.IOException;

public class DataImporter {

	public static void importData(String file) throws IOException{
		new CSVReader(new File(file)).insertArticleFile();
	}
	
}
