package de.newscluster.service.hibernate;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.context.internal.ManagedSessionContext;

public class SessionManager {

	protected static Session session;

	public SessionManager() {
	
	}
	
	protected synchronized void openTransaction() {
		session = HibernateUtil.getSessionAnnotationFactory().openSession();
		session.setFlushMode(FlushMode.MANUAL);
		ManagedSessionContext.bind(session);
		session.beginTransaction();
	}

	protected synchronized void closeTransaction() {
		ManagedSessionContext.unbind(HibernateUtil.getSessionAnnotationFactory());
		if (session.isOpen()) {
			session.flush();
			session.getTransaction().commit();
			session.close();
		}
	}

}
