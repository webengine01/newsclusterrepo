package de.newscluster.service.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import de.newscluster.model.Article;
import de.newscluster.model.Comment;
import de.newscluster.model.Contact;
import de.newscluster.model.Image;
import de.newscluster.model.Link;
import de.newscluster.model.Rank;
import de.newscluster.model.Topic;
import de.newscluster.model.Track;
import de.newscluster.model.User;

public class HibernateUtil {

	private static void mapObjects(Configuration configuration) {
		// Add OR mapped classes to configuration
		configuration.addAnnotatedClass(Article.class);
		configuration.addAnnotatedClass(Comment.class);
		configuration.addAnnotatedClass(Image.class);
		configuration.addAnnotatedClass(Rank.class);
		configuration.addAnnotatedClass(Topic.class);
		configuration.addAnnotatedClass(Track.class);
		configuration.addAnnotatedClass(User.class);
		configuration.addAnnotatedClass(Contact.class);
		configuration.addAnnotatedClass(Link.class);
		configuration.addAnnotatedClass(User.class);

	}

	// Annotation based configuration
	private static SessionFactory sessionAnnotationFactory;

	private static SessionFactory buildSessionAnnotationFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			configuration.configure("hibernate.cfg.xml");
			mapObjects(configuration);
			System.out.println("Hibernate Annotation Configuration loaded");
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).build();
			System.out.println("Hibernate Annotation serviceRegistry created");

			SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			return sessionFactory;
		}
		

		catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	

	
	public static SessionFactory getSessionAnnotationFactory() {
		if (sessionAnnotationFactory == null)
			sessionAnnotationFactory = buildSessionAnnotationFactory();
		return sessionAnnotationFactory;
	}
}