package de.newscluster.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.cache.annotation.Cacheable;

import de.newscluster.model.Topic;
import de.newscluster.service.hibernate.SessionManager;

public class TopicService extends SessionManager {

	private static TopicService instance;
	
	private TopicService() {
		// Empty private singleton constructor
	}

	public static TopicService getInstance() {
		if (instance == null) {
			instance = new TopicService();
		}
		return instance;
	}

	@Cacheable
	public Topic retrieveByID(int id) {
		return retrive("FROM Topic where topicID=" + id).get(0);
	}

	@Cacheable
	public ArrayList<Topic> retriveByRank(int rank) {
		return retrive("FROM Topic where rank=" + rank);
	}

	@SuppressWarnings("unchecked")
	private ArrayList<Topic> retrive(String baseQuery) {
		this.openTransaction();
		Query query = session.createQuery(baseQuery);
		List<?> res = query.list();
		this.closeTransaction();
		return (ArrayList<Topic>) res;
	}
}
