package de.newscluster.service;

import java.util.List;
import org.hibernate.Query;
import org.springframework.cache.annotation.Cacheable;

import de.newscluster.model.User;
import de.newscluster.service.hibernate.SessionManager;

public class UserService extends SessionManager {
	
	private static UserService instance;

	private UserService() {

	}

	public static UserService getInstance() {
		if (instance == null) {
			instance = new UserService();
		}
		return instance;
	}

	public void insert(String name, String password, String email) {
		this.openTransaction();
		session.save(new User(name, password, email));
		this.closeTransaction();
	}

	public void insertUser(User user) {
		this.openTransaction();
		System.out.println("USER DATA: " + user.getEmail() + " " + user.getName() + " " + user.getPassword());
		session.save(user);
		this.closeTransaction();
	}
	
	@SuppressWarnings("rawtypes")
	public List getUserList(){
		this.openTransaction();
		String qString = "FROM User";
		Query query = session.createQuery(qString);
		query = session.createQuery(qString);
		List l = query.list();
		this.closeTransaction();
		return l;
	}
	
	public boolean update(User u){
		openTransaction();
		session.update(u);
		closeTransaction();
		return true;
		
	}
	
	public boolean maunallyUpdate(User u){
		openTransaction();
		String qString = "FROM User where email='" + u.getEmail() + "'";
		Query query = session.createQuery(qString);
		query = session.createQuery(qString);
		if (query.list().size() > 0) {
			return false;
		}
		qString = "FROM User where name='" + u.getName() + "'";
		query = session.createQuery(qString);
		query = session.createQuery(qString);
		if (query.list().size() > 0) {
			return false;
		}
		session.update(u);
		closeTransaction();
		return true;	
	}
	
	public User getRegisteredUser(String name, String password){
		this.openTransaction();
		String qString = "FROM User where email='" + name + "' and password='" + password + "'";
		Query query = session.createQuery(qString);
		query = session.createQuery(qString);
		User user = null;
		if (query.list() != null && query.list().size()> 0) {
			user = (User) query.list().get(0);
		}
		this.closeTransaction();
		return user;
	}
	
//	public User getUser(String email){
//		this.openTransaction();
//		//FROM \"User\" 
//		//TODO: Increase security
//		String qString = "FROM User where email=" + email;
//		Query query = session.createQuery(qString);
//		query = session.createQuery(qString);
//		User user = (User) query.list().get(0);
//		this.closeTransaction();
//		return user;
//	}

	@Cacheable
	public User getUser(int usrId) {
		this.openTransaction();
		String qString = "FROM User where usrID=" + usrId;
		Query query = session.createQuery(qString);
		query = session.createQuery(qString);
		List<?> results = query.list();
		User usr = (User) results.get(0);
		this.closeTransaction();
		return usr;
	}

}
