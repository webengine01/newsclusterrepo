package de.newscluster.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.cache.annotation.Cacheable;

import de.newscluster.model.Article;
import de.newscluster.model.Image;
import de.newscluster.model.Link;
import de.newscluster.model.Rank;
import de.newscluster.model.Topic;
import de.newscluster.service.hibernate.SessionManager;

public class ArticleService extends SessionManager {

	private static ArticleService instance;

	private ArticleService() {
		// Empty private singleton constructor
	}

	public static ArticleService getInstance() {
		if (instance == null) {
			instance = new ArticleService();
		}
		return instance;
	}

	public void addRecord(Object o) {
		session.save(o);
	}

	private boolean isPresent(List<?> l) {
		if (l != null) {
			if (l.size() > 0) {
				return true;
			}
		}
		return false;
	}

	public void insert(String headLine, String content, String date, String tags, String imgPath, int score, String top,
			String topTags, String sLink) {
		Article article = new Article(headLine, content, new Date());
		Rank rank = new Rank(score);
		Topic topic = new Topic(top, topTags);
		Image img = new Image(imgPath);
		Link link = new Link();
		this.openTransaction();
		
		// verify existence of rank object containing current rank
		String qString = "FROM Rank where rank=" + rank.getRank();
		Query query = session.createQuery(qString);
		List<?> results = query.list();
		
		if (isPresent(results)) {
			rank = (Rank) results.get(0);
		} else {
			addRecord(rank);
		}
		qString = "FROM Topic where tags='" + topic.getTags() + "'";
		query = session.createQuery(qString);
		results = query.list();
		
		if (isPresent(results)) {
			topic = (Topic) results.get(0);
			addRecord(topic);
		}
		article.setTags(topic.getTags());
		article.setRankID(rank);
		article.setTopicID(topic);
		addRecord(article);
		
		qString = "FROM Image where path='" + img.getPath() + "' AND artID=" + img.getArtID();
		query = session.createQuery(qString);
		results = query.list();
		
		if (!isPresent(results)) {
			img.setArtID(article);
			addRecord(img);
		}
		
		qString = "FROM Link where link='" + link.getLink() + "'";
		query = session.createQuery(qString);
		results = query.list();
		
		if (!isPresent(results)) {
			link.setArtID(article);
			addRecord(img);
		}
		
		this.closeTransaction();
	}

	@SuppressWarnings("unused")
	private List<?> query(String query) {
		return session.createQuery(query).list();
	}

	
	public ArrayList<Article> retriveAll() {
		return retrive("From Article");
	}
	
	@Cacheable
	public Article retriveByID(int artId) {
		return retrive("From Article where artID = " + artId).get(0);
	}
	@Cacheable
	public ArrayList<Article> retriveByTopic(int topicID) {
		return retrive("From Article where topicID = " + topicID);
	}
	@Cacheable
	private ArrayList<Article> retrive(String baseQuery) {
		this.openTransaction();
		ArrayList<Article> artList = new ArrayList<Article>();
		String qString = baseQuery;
		System.out.println(qString);
		Query query = session.createQuery(qString);
		List<?> res = query.list();
		for (Object object : res) {
			Article art = (Article) object;
			// query topic
			System.out.println("DEBUG: " + art.getTopicID());
			qString = "FROM Topic where topicID=" + art.getTopicID().getTopicID();
			query = session.createQuery(qString);
			List<?> results = query.list();
			art.setTopicID((Topic) results.get(0));
			// query rank
//			qString = "FROM Rank where rankID=" + art.getRankID().getRankID();
//			query = session.createQuery(qString);
//			results = query.list();
//			art.setRankID((Rank) results.get(0));
			// query img
			qString = "FROM Image where artID=" + art.getArtID();
			query = session.createQuery(qString);
			results = query.list();
			System.out.println(results.size());
			art.setImg((Image) results.get(0));
			//query link
			qString = "FROM Link where artID=" + art.getArtID();
			query = session.createQuery(qString);
			results = query.list();
			art.setLink((Link) results.get(0));
			// adding article to list
			artList.add(art);
		}
		this.closeTransaction();
		return artList;
	}

}
