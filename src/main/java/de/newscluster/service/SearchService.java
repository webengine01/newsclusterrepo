package de.newscluster.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import de.newscluster.model.Article;
import de.newscluster.service.hibernate.SessionManager;

public class SearchService extends SessionManager {

	private static SearchService instance;

	@SuppressWarnings("unused")
	private static Object monitor;

	private SearchService() {
		monitor = new Object();
	}
	
	public static SearchService getInstance() {
		if (instance == null) {
			instance = new SearchService();
		}
		return instance;
	}
/*
	private Runnable getSearchThread(List<Article> results, String baseQuery, String key, String value) {
		Runnable r = new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				openTransaction();
				Query query = session.createQuery(baseQuery);
				List<?> artList = query.setParameter(key, value).list();
				synchronized (monitor) {
					results.addAll((List<Article>) artList);
				}
				closeTransaction();
			}
		};
		return r;
	}
*/
	
	@SuppressWarnings("unchecked")
	private void execSearchQuery(List<Article> results, String[] baseQuery, String key, String value) {
		openTransaction();
		for (int i = 0; i < baseQuery.length; i++) {
			Query query = session.createQuery(baseQuery[i]);
			List<?> artList = query.setParameter(key, value).list();
			results.addAll((List<Article>) artList);
		}
		closeTransaction();
	}

	@SuppressWarnings("unchecked")
	public List<Article> searchByTags(String[] tags){
		List<Article> searchResults = new ArrayList<Article>();
		String baseQuery = "From Article where tags like :key";
		openTransaction();
		for (String tag : tags) {
			Query query = session.createQuery(baseQuery);
			List<?> artList = query.setParameter("key", "%" + tag + "%").list();
			searchResults.addAll((List<Article>) artList);
		}
		closeTransaction();
		return searchResults;
	}
	
	public List<Article> search(String text) throws InterruptedException {
		List<Article> searchResults = new ArrayList<Article>();
		String[] baseQuerys = { "From Article where headLine like :key", "From Article where content like :key",
				"From Article where tags like :key" };
		String key = "key";
		execSearchQuery(searchResults, baseQuerys, key, "%" + text + "%");
		return searchResults;
	}

	/*
	 * public List<Article> search(String text) throws InterruptedException {
	 * List<Article> searchResults = new ArrayList<Article>(); ArrayList<Thread>
	 * tList = new ArrayList<Thread>(); String key = "key"; tList.add(new
	 * Thread( getSearchThread(searchResults,
	 * "From Article where headLine like :key", key, "%" + text + "%")));
	 * tList.add(new Thread( getSearchThread(searchResults,
	 * "From Article where content like :key", key, "%" + text + "%")));
	 * tList.add( new Thread(getSearchThread(searchResults,
	 * "From Article where tags like :key", key, "%" + text + "%"))); // start
	 * search threads for (Thread thread : tList) { thread.start(); } // join
	 * search threads for (Thread thread : tList) { thread.join(); } // returns
	 * search results return searchResults; }
	 */

}
