package de.newscluster.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.Query;
import org.springframework.cache.annotation.Cacheable;

import de.newscluster.model.Comment;
import de.newscluster.model.Topic;
import de.newscluster.service.hibernate.SessionManager;

public class CommentService extends SessionManager {

	private static CommentService instance;

	private CommentService() {
	}

	public static CommentService getInstance() {
		if (instance == null) {
			instance = new CommentService();
		}
		return instance;
	}

	public void insert(int topicID, String content) {
		this.openTransaction();
		Comment com = new Comment();
		String qString = "FROM Topic where topic_id=" + topicID;
		Query query = session.createQuery(qString);
		com.setTopic((Topic) query.list().get(0));
		com.setContent(content);
		session.save(com);
		this.closeTransaction();
	}

	public void insert(Comment comment) {
		this.openTransaction();
		session.save(comment);
		this.closeTransaction();
	}

	@Cacheable
	public ArrayList<Comment> retriveByID(int topicID) {

		// TODO Ergebnisse absteigend ausliefern (neueste ganz oben!)

		this.openTransaction();
		ArrayList<Comment> comList = new ArrayList<Comment>();
		String qString = "FROM Comment where topic_id=" + topicID;
		Query query = session.createQuery(qString);
		List<?> l = query.list();
		for (Object obj : l) {
			comList.add((Comment) obj);
		}

		this.closeTransaction();

		Collections.sort(comList, new Comparator<Comment>() {
			@Override
			public int compare(Comment c1, Comment c2) {
				return c1.getDate().compareTo(c2.getDate());
			}
		});

		return comList;

	}
}
