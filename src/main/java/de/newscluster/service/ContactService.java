package de.newscluster.service;

import de.newscluster.model.Contact;
import de.newscluster.service.hibernate.SessionManager;

public class ContactService extends SessionManager{
	
	private static ContactService instance;
	
	private ContactService() {
	}
	
	public static ContactService getInstance(){
		if (instance == null) {
			instance = new ContactService();
		}
		return instance;
	}
	
	public void insert(Contact con){
		openTransaction();
		session.save(con);
		closeTransaction();
		
	}

}
