package de.newscluster.controller.helper;

public class FBShare {

	public String value = "https://www.facebook.com/sharer/sharer.php?u=";

	public FBShare(String url) {
		value += url;
	}

	public String getFBShare() {
		return value;
	}
}
