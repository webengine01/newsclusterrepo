package de.newscluster.controller.helper;

public class Tag {

	private String tag;

	public Tag(String tag) {
		this.setTag(tag);
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	
}
