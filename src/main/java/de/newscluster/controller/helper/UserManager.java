package de.newscluster.controller.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.newscluster.model.User;
import de.newscluster.service.UserService;

public class UserManager {

	public static final User getAssociatedUser(String cookieUser) {
		User pageUser = null;
		if (!cookieUser.equals("defaultUser")) {
			pageUser = UserService.getInstance().getUser(Integer.parseInt(cookieUser));
			pageUser.setAuthentication(true);
		} else {
			pageUser = new User();
			pageUser.setAuthentication(false);
		}
		
		return pageUser;
	}
	
	public static String getDateStringFromMillis(long timestamp) {
		Date date = new Date(timestamp);
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy > hh:mm");
		return df.format(date);
		
	}
}
