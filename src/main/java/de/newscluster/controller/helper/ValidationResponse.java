package de.newscluster.controller.helper;

import java.util.List;

/*
 * the ajax response will
 * converted into json from spring 
 */

public class ValidationResponse {

	private String status;
	private List errorMessageList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List getErrorMessageList() {
		return this.errorMessageList;
	}

	public void setErrorMessageList(List errorMessageList) {
		this.errorMessageList = errorMessageList;
	}

}
