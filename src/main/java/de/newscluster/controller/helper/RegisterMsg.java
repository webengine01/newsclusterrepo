package de.newscluster.controller.helper;

public class RegisterMsg {
	
	private boolean isRegSuccessfull;
	private String regMessage;
	
	public boolean isRegSuccessfull() {
		return isRegSuccessfull;
	}

	public void setRegSuccessfull(boolean isRegSuccessfull) {
		this.isRegSuccessfull = isRegSuccessfull;
	}

	public String getRegMessage() {
		return regMessage;
	}

	public void setRegMessage(String regMessage) {
		this.regMessage = regMessage;
	}

}
