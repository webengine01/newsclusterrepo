package de.newscluster.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import de.newscluster.controller.helper.LoginMsg;
import de.newscluster.model.Topic;
import de.newscluster.model.Track;
import de.newscluster.model.User;
import de.newscluster.service.TopicService;
import de.newscluster.service.TrackService;

@Controller
public class UserDashBoardController {

	@RequestMapping("/userdashboard")
	public String showDashBoard(Model model, HttpServletRequest request) {
		LoginMsg msg = null;

		if (request.isRequestedSessionIdValid()) {
			msg = new LoginMsg(0, "Willkommen zurück!");
			User user = (User) request.getSession(false).getAttribute("user");

			if (user == null || !request.isRequestedSessionIdValid()) {
				model.addAttribute("loginmsg", new LoginMsg(3, "Login erforderlich!"));
				return "userdashboard";
			}

			List<Topic> topicList = TrackService.getInstance().retriveByUser(user.getUsrID());
			model.addAttribute("loginmsg", msg);

			if (topicList == null) {
				topicList = new ArrayList<>();
			}

			model.addAttribute("topicList", topicList);
		}
		

		if (msg == null) {
			msg = new LoginMsg(1, "Login-Versuch war leider nicht erfolgreich!");
			model.addAttribute("loginmsg", msg);
		}

		return "userdashboard";
	}

	// mit window.location.href kein POST möglich
	// workaround mit form möglich

	@RequestMapping(value = "/deletefeed={topic}", method = RequestMethod.GET)
	public String deleteFeed(@PathVariable("topic") String topic, Model model, HttpServletRequest request) {

		System.out.println("CALLED THE DELETE FEED CONTROLLER");

		int topicID = Integer.valueOf(topic);

		if (!request.isRequestedSessionIdValid()) {
			model.addAttribute("loginmsg", new LoginMsg(1, "Login erforderlich!"));
			return "userdashboard";
		}

		Topic retrievedTopic = TopicService.getInstance().retrieveByID(topicID);
		User retrievedUser = (User) request.getSession(true).getAttribute("user");

		TrackService.getInstance().deleteTrackByUserAndTopic(topicID, retrievedUser.getUsrID());
		LoginMsg msg = new LoginMsg(0, "Das Thema '" + retrievedTopic.getTopic() + "' wurde von Ihren Feeds gelöscht!");
		List<Topic> topicList = TrackService.getInstance().retriveByUser(retrievedUser.getUsrID());

		if (topicList == null) {
			topicList = new ArrayList<>();
		}

		model.addAttribute("topicList", topicList);
		model.addAttribute("loginmsg", msg);

		// redirect:/
		return "userdashboard";
	}

	@RequestMapping("/follow={key}")
	public String followTopic(@PathVariable("key") String key, Model model, HttpServletRequest request) {

		User user = (User) request.getSession(false).getAttribute("user");

		if (user == null || !request.isRequestedSessionIdValid()) {
			model.addAttribute("loginmsg", new LoginMsg(3, "Login erforderlich!"));
			return "userdashboard";
		}

		System.out.println("USER:: " + user.getUsrID());
		Track track = new Track();
		track.setUsrID(user);
		track.setTopicID(TopicService.getInstance().retrieveByID(Integer.parseInt(key)));
		TrackService.getInstance().insert(track);
		List<Topic> topicList = TrackService.getInstance().retriveByUser(user.getUsrID());
		model.addAttribute("topicList", topicList);
		model.addAttribute("loginmsg", new LoginMsg(0, "Das Thema " + key + " wurde abonniert"));
		return "userdashboard";
	}
}
