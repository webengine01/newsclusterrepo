package de.newscluster.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.newscluster.controller.helper.LoginMsg;
import de.newscluster.model.Topic;
import de.newscluster.model.User;
import de.newscluster.model.forms.RegisterForm;
import de.newscluster.service.TrackService;
import de.newscluster.service.UserService;

@Controller
public class EditUserDataController {

	@RequestMapping(value = "/edituserdata", method = RequestMethod.GET)
	public ModelAndView editUserData(Model model, HttpServletRequest request) {
		User user = (User) request.getSession(false).getAttribute("user");

		if (user == null || !request.isRequestedSessionIdValid()) {
			return new ModelAndView("userdashboard", "loginmsg", new LoginMsg(1, "Login erforderlich!"));
		}

		RegisterForm userForm = new RegisterForm(user.getName(), user.getPassword(), user.getEmail());

		return new ModelAndView("edituserdata", "newUser", userForm);
	}

	@RequestMapping(value = "/edituserdata", method = RequestMethod.POST)
	public String showDashBoardAfterEdit(@Valid @ModelAttribute("newUser") RegisterForm newUser,
			BindingResult bindingResult, HttpServletRequest request, Model model) {
		if (bindingResult.hasErrors()) {
			return "edituserdata";
		}
		User currentUser = (User) request.getSession(true).getAttribute("user");
		currentUser.setPassword(RegisterController.hashPassword(newUser.getPassword()));
		currentUser.setEmail(newUser.getEmail());
		currentUser.setName(newUser.getName());
		UserService.getInstance().update(currentUser);
		LoginMsg msg = new LoginMsg(0, "Die Daten wurden geändert!");
		List<Topic> topicList = TrackService.getInstance().retriveByUser(currentUser.getUsrID());

		if (topicList == null) {
			topicList = new ArrayList<>();
		}

		model.addAttribute("topicList", topicList);
		model.addAttribute("loginmsg", msg);
		return "userdashboard";
	}
	
}
