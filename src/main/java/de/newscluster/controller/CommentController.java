package de.newscluster.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import de.newscluster.controller.helper.ValidationResponse;
import de.newscluster.model.Comment;
import de.newscluster.model.User;
import de.newscluster.model.forms.CommentForm;
import de.newscluster.service.CommentService;
import de.newscluster.service.TopicService;

@Controller
public class CommentController {

	@RequestMapping(value = "/comments/{topicid}&name={namekey}&email={mailkey}&content={contentkey}", method = RequestMethod.GET)
	public String insertNewComment(@PathVariable int topicid, @PathVariable String namekey,
			@PathVariable String mailkey, @PathVariable String contentkey, Model model) {

		System.out.println("DEBUG : " + topicid);
		System.out.println("DEBUG : " + namekey);
		System.out.println("DEBUG : " + mailkey);
		System.out.println("DEBUG : " + contentkey);

		User user = new User(namekey, "hallihallo", mailkey);

		// Datum wird im Comment Konstruktor erzeugt
		// die ID kommt safe über thymeleaf

		Comment c = new Comment(contentkey, TopicService.getInstance().retrieveByID(topicid), user);
		CommentService.getInstance().insert(c);
		
		model.addAttribute("comment", c);
		return "fragments/comments :: newcomment";
	}

	@RequestMapping(value = "/newcomment", method = RequestMethod.POST)
	public @ResponseBody ValidationResponse processForm(@ModelAttribute(value = "commentForm") CommentForm commentForm,
			BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

		System.out.println("DEBUG :: AJAX CALL TO LOGIN CONTROLLER");
		System.out.println("DEBUG :: NAME = " + commentForm.getName());
		
		System.out.println("DEBUG :: MAIL = " + commentForm.getEmail());

		System.out.println("DEBUG :: CONTENT = " + commentForm.getContent());
		ValidationResponse valResponse = new ValidationResponse();

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();

		Set<ConstraintViolation<CommentForm>> constraintViolations = validator.validate(commentForm);

		// Add JSR-303 errors to BindingResult
		// This allows Spring to display them in view via a FieldError
		constraintViolations.forEach(violation -> {
			String propertyPath = violation.getPropertyPath().toString();
			String message = violation.getMessage();
			bindingResult.addError(new FieldError("commentForm", propertyPath, violation.getMessage()));
		});

		if (bindingResult.hasErrors()) {
			valResponse.setStatus("FAIL");
			valResponse.setErrorMessageList(bindingResult.getAllErrors());
		} else {
			System.out.println("###### DEBUG :: DER KOMMENTAR IST IN ORDNUNG -> MUSS PERSISTIERT WERDEN!");
			valResponse.setStatus("SUCCESS");
		}

		return valResponse;
	}

}
