package de.newscluster.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.newscluster.model.Contact;
import de.newscluster.model.forms.ContactForm;
import de.newscluster.service.ContactService;

@Controller
public class ContactRequestController {

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public ModelAndView contactRequest() {
		return new ModelAndView("contact", "contact", new ContactForm());
	}

	@RequestMapping(value = "/contact", method = RequestMethod.POST)
	public String addNewContactRequest(@Valid @ModelAttribute("contact") ContactForm contactForm,
			BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			return "contact";
		}

		Contact newContact = new Contact(contactForm.getContent(), contactForm.getEmail(), contactForm.getName());

		ContactService.getInstance().insert(newContact);
		model.addAttribute("contact", newContact);
		return "submitContactReq";
	}

}
