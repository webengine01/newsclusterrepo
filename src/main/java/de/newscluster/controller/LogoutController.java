package de.newscluster.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import de.newscluster.controller.helper.LoginMsg;

@Controller
public class LogoutController {

	@RequestMapping("/logout")
	public String logOut(Model model, HttpServletRequest request) {
		LoginMsg msg = new LoginMsg(2, "Auf wiedersehen ");
		model.addAttribute("loginmsg", msg);

		HttpSession session = request.getSession(false);

		if (session != null) {
			session.invalidate();
		}

		return "userdashboard";
	}

}
