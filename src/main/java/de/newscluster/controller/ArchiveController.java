package de.newscluster.controller;


import de.newscluster.controller.helper.Tag;
import de.newscluster.model.Article;
import de.newscluster.service.ArticleService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;



@Controller 
public class ArchiveController {

	private static final String DATE_FORMAT = "dd.MM.yyyy";
	
	private Set<Tag> tagSet;
	private ArrayList<Article> artList;
	
	@RequestMapping("/archive")
	public String index(Model m) {
		initArtList();
		initTagList(artList); 
		m.addAttribute("orderedArticles", artList);
		m.addAttribute("tagList", tagSet);	
		return "archive";
	}
	
	
	
	private void initArtList(){
		if (artList == null) {
			artList = ArticleService.getInstance().retriveAll();
			Collections.sort(artList, new Comparator<Article>() {
				public int compare(Article a1, Article a2) {
					return a1.getDate().compareTo(a2.getDate());
				}
			});
		}
	}
	
	private void initTagList(ArrayList<Article> artList){
		if (tagSet == null) {
			tagSet = new HashSet<Tag>();
			for (Article article : artList) {
				String[] split = article.getTags().split(",");
				for (String s : split) {
					tagSet.add(new Tag(s.trim()));
				}
			}
		}
	}
	
	public  boolean isInRange(Date startDate, Date endDate, Date currentDate){
		return (currentDate.after(startDate) && currentDate.before(endDate) || (startDate.compareTo(currentDate)== 0 && endDate.compareTo(currentDate) == 0));
	}

	//Create DateUtil class
	private boolean isParseableDate(String date){
		System.out.println("THIS:: " + date);
		if (date == null || date.isEmpty() || date.equals("") ) {
			return false;
		}
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		try {
			dateFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		} 
		return true;
	}
	
	@RequestMapping(value = "/archive={tag}from={from}to={to}&", method = RequestMethod.GET)
	public String archSearch(@PathVariable("tag") String tag, @PathVariable("from") String from, @PathVariable("to") String to, Model m){
		if (artList == null) {
			initArtList();
		}
		if (tagSet == null) {
			initTagList(artList);
		}
		
		if (isParseableDate(from) && isParseableDate(to)) {
			//Move to DateUtil class
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			Date fromDate = null;
			Date toDate = null;
			try {
				fromDate = dateFormat.parse(from);
				toDate = dateFormat.parse(to);
			} catch (ParseException e) {
				e.printStackTrace();
			} 
			ArrayList<Article> resArticles = new ArrayList<Article>();
			for (Article article : artList) {
				if (article.getTags().contains(tag)) {
					if (isInRange(fromDate, toDate, article.getDate())) {
						resArticles.add(article);
					}
				}
			}

			System.out.println("Returned processed");
			m.addAttribute("orderedArticles", resArticles);
		} else {
			System.out.println("Returned default");
			
			m.addAttribute("orderedArticles", artList);
		}
		m.addAttribute("tagList", tagSet);
		return "archive";
	}

}