package de.newscluster.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;

import de.newscluster.controller.helper.UserManager;
import de.newscluster.model.Article;
import de.newscluster.model.Topic;
import de.newscluster.service.ArticleService;
import de.newscluster.service.TopicService;

@Controller
public class IndexController {

	private ArrayList<Article> highPrioArticles;
	private ArrayList<Article> midPrioArticles;

	// Multiple mappings for index
	@RequestMapping(value = { "/", "/index" })
	public String index(@CookieValue(value = "user", defaultValue = "defaultUser") String cookieUser, Model model,
			HttpServletResponse response) {

		if (highPrioArticles == null) {
			fetchHighPrioContent(1);
		}
		if (midPrioArticles == null) {
			fetchMidPrioContent(2);
		}
		model.addAttribute("highPrioArticles", highPrioArticles);
		model.addAttribute("midPrioArticles", midPrioArticles);
		model.addAttribute("user", UserManager.getAssociatedUser(cookieUser));
		return "index";
	}

	private void fetchHighPrioContent(int rank) {
		// get highest rated articles
		highPrioArticles = ArticleService.getInstance()
				.retriveByTopic(TopicService.getInstance().retriveByRank(rank).get(0).getTopicID());
	}


	private void fetchMidPrioContent(int rank) {
		// get highest rated articles
		// TODO: Implement proper solution for this
		midPrioArticles = new ArrayList<Article>();
		List<Topic> top = TopicService.getInstance().retriveByRank(rank);
		for (Topic topic : top) {
			List<Article> art = ArticleService.getInstance().retriveByTopic(topic.getTopicID());
			for (Article article : art) {
				midPrioArticles.add(article);
			}
		}
	}
}