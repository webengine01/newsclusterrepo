package de.newscluster.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.newscluster.model.forms.PasswordLostForm;


/*
 * 
 * KEINE PRIO!
 * 
 * email mit neuem passwort senden,
 * wird es vermutlich irgendwie mit spring-security schon geben * 
 * 
 */

@Controller
public class PasswordLostController {

	@RequestMapping(value = "/passwordlost", method = RequestMethod.GET)
	public ModelAndView showForm() {
		return new ModelAndView("passwordlost", "passWordForm", new PasswordLostForm());
	}

	@RequestMapping(value = "/passwordlost", method = RequestMethod.POST)
	public String processForm(@Valid @ModelAttribute("passWordForm") PasswordLostForm form,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			System.out.println("DEBUG :: BINDING RESULT HAS ERROR!");
			return "passwordlost";
		}
		// TODO: send Mail to User with a new password
		return "submitpasswordlost";
	}

}
