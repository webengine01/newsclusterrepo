package de.newscluster.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import de.newscluster.controller.helper.LoginMsg;
import de.newscluster.controller.helper.ValidationResponse;
import de.newscluster.model.Article;
import de.newscluster.model.User;
import de.newscluster.model.forms.LoginForm;
import de.newscluster.model.forms.SearchForm;
import de.newscluster.service.SearchService;
import de.newscluster.service.UserService;

@Controller
public class SearchController {

	 @RequestMapping("/search={key}")
	 public String search(@PathVariable("key") String key, Model model) {
	 List<Article> resList = null;
	 if (key.contains("&")) {
	 String[] tagSplit = key.split("&");
	 resList = (ArrayList<Article>)
	 SearchService.getInstance().searchByTags(tagSplit);
	 } else {
	 try {
	 resList = (ArrayList<Article>) SearchService.getInstance().search(key);
	 } catch (InterruptedException e) {
	 e.printStackTrace();
	 }
	 }
	 if (resList == null || resList.size() == 0) {
	 // TODO Handle no result on site
	 }
	
	 for (Article article : resList) {
	 System.out.println("DEBUG:: " + article.getHeadLine());
	 System.out.println("DEBUG:: " + article.getContent());
	 }
	 model.addAttribute("searchResultArticles", new HashSet<>(resList));
	 return "searchResults";
	 }

	 
	 
	@RequestMapping(value = "/validatesearch", method = RequestMethod.POST)
	public @ResponseBody ValidationResponse processForm(@ModelAttribute(value = "searchForm") SearchForm searchForm,
			BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

		System.out.println("DEBUG :: AJAX CALL TO SEARCHCONTROLLER");
		System.out.println("DEBUG :: " + searchForm.getContent());
		

		ValidationResponse valResponse = new ValidationResponse();

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();

		Set<ConstraintViolation<SearchForm>> constraintViolations = validator.validate(searchForm);

		// Add JSR-303 errors to BindingResult
		// This allows Spring to display them in view via a FieldError
		constraintViolations.forEach(violation -> {
			String propertyPath = violation.getPropertyPath().toString();
			bindingResult.addError(new FieldError("searchForm", propertyPath, violation.getMessage()));
		});

		if (bindingResult.hasErrors()) {
			valResponse.setStatus("FAIL");
			valResponse.setErrorMessageList(bindingResult.getAllErrors());
		} else {
			valResponse.setStatus("SUCCESS");
		}

		return valResponse;
	}

//	@RequestMapping(value = "/search", method = RequestMethod.GET)
//	private ModelAndView processSearch(String key) {
//
//		System.out.println("DEBUG :: PROCESS SEARCH");		
//		List<Article> resList = null;
//
//		if (key.contains("&")) {
//			String[] tagSplit = key.split("&");
//			resList = (ArrayList<Article>) SearchService.getInstance().searchByTags(tagSplit);
//		} else {
//			try {
//				resList = (ArrayList<Article>) SearchService.getInstance().search(key);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
//
//		if (resList == null || resList.size() == 0) {
//			// TODO Handle no result on site
//		}
//
//		for (Article article : resList) {
//			System.out.println("DEBUG:: " + article.getHeadLine());
//			System.out.println("DEBUG:: " + article.getContent());
//		}
//		
//		return new ModelAndView("searchResults", "searchResultArticles", new HashSet<>(resList));
//
//	}
}
