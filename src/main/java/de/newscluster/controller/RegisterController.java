package de.newscluster.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.newscluster.controller.helper.RegisterMsg;
import de.newscluster.controller.helper.VerifyRecaptcha;
import de.newscluster.model.User;
import de.newscluster.model.forms.RegisterForm;
import de.newscluster.service.UserService;

@Controller
public class RegisterController {

	/**
	 * For User registration
	 * 
	 * @return
	 */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView showForm() {
		return new ModelAndView("register", "newUser", new RegisterForm());
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String addUser(@Valid @ModelAttribute("newUser") RegisterForm newUser, BindingResult bindingResult,
			Model model, HttpServletRequest request, HttpServletResponse response) {

//		// get reCAPTCHA request param
//		String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
//		System.out.println(gRecaptchaResponse);
//
//		boolean verify = false;
//
//		try {
//			verify = VerifyRecaptcha.verify(gRecaptchaResponse);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		if (bindingResult.hasErrors()) {
			System.out.println("DEBUG :: BINDING RESULT HAS ERROR!");
			return "register";
		} else {
			User user = new User(newUser.getName(), newUser.getPassword(), newUser.getEmail());
			model.addAttribute("regmsg", checkUserData(user));
			return "successfullRegistration";
		}
	}

	private static RegisterMsg checkUserData(User user) {

		@SuppressWarnings("unchecked")
		List<User> fetchedUsers = UserService.getInstance().getUserList();
		RegisterMsg msg = new RegisterMsg();
		msg.setRegMessage(null);
		for (User usr : fetchedUsers) {
			if (user.getEmail().equals(usr.getEmail())) {
				msg.setRegSuccessfull(false);
				msg.setRegMessage("Ein Benutzer mit dieser Mail-Adresse exisitert bereits.");
				break;
			}
			if (user.getName().equals(usr.getName())) {
				msg.setRegSuccessfull(false);
				msg.setRegMessage("Ein Benutzer mit diesem Namen exisitert bereits.");
			}
		}
		if (msg.getRegMessage() == null) {
			msg.setRegSuccessfull(true);
			msg.setRegMessage("Vielen Dank für deine Registrierung");
			System.out.println("password:::" + user.getPassword());
			String pw = user.getPassword();
			user.setPassword(hashPassword(pw));
			UserService.getInstance().insertUser(user);
		}
		return msg;
	}

	public static String hashPassword(String password) {
		int workload = 12;
		String salt = BCrypt.gensalt(workload);
		return BCrypt.hashpw(password, salt);
	}

}
