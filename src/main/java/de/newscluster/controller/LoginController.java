package de.newscluster.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import de.newscluster.controller.helper.LoginMsg;
import de.newscluster.controller.helper.ValidationResponse;
import de.newscluster.model.User;
import de.newscluster.model.forms.LoginForm;
import de.newscluster.service.UserService;

@Controller
public class LoginController {

	// @ResponseBody, not necessary, since class is annotated with
	// @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped
	// by field name.

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody ValidationResponse processForm(@ModelAttribute(value = "loginForm") LoginForm loginForm,
			BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

		System.out.println("DEBUG :: AJAX CALL TO LOGINCONTROLLER");
		System.out.println("DEBUG :: " + loginForm.getEmail());
		System.out.println("DEBUG :: " + loginForm.getPassword());

		ValidationResponse valResponse = new ValidationResponse();

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();

		Set<ConstraintViolation<LoginForm>> constraintViolations = validator.validate(loginForm);

		// Add JSR-303 errors to BindingResult
		// This allows Spring to display them in view via a FieldError
		constraintViolations.forEach(violation -> {
			String propertyPath = violation.getPropertyPath().toString();
			bindingResult.addError(new FieldError("loginForm", propertyPath, violation.getMessage()));

		});

		if (bindingResult.hasErrors()) {
			valResponse.setStatus("FAIL");
			valResponse.setErrorMessageList(bindingResult.getAllErrors());
		} else {
			processLogin(loginForm.getEmail(), loginForm.getPassword(), response, request);
			valResponse.setStatus("SUCCESS");
		}

		return valResponse;
	}

	private ModelAndView processLogin(String email, String password, HttpServletResponse response,
			HttpServletRequest request) {

		LoginMsg msg = null;
		
		ModelAndView returnModelAndView = new ModelAndView();
		returnModelAndView.setViewName("userdashboard");

		@SuppressWarnings("unchecked")
		List<User> fetchedUsers = UserService.getInstance().getUserList();

		for (User user : fetchedUsers) {
			if (checkPassword(password, user.getPassword())) {
				if (user.getEmail().equals(email)) {
					// Found user for specified attributes

					HttpSession session = request.getSession();
					session.setAttribute("user", user);
					session.setMaxInactiveInterval(30 * 60);

					msg = new LoginMsg(0, "Willkommen zurück " + user.getName());
					returnModelAndView.addObject("loginmsg", msg);
					break;

				}
			}
		}

		if (msg == null) {
			msg = new LoginMsg(1, "Passwort oder E-Mail Adresse falsch!");
		}

		System.out.println("DEBUG :: LoginMsg=" + msg.getMsg() + " | LoginStatus=" + msg.getStatus());
		System.out.println("DEBUG :: returned the modealandview -> processLogin()");
		returnModelAndView.addObject("loginmsg", msg);

		return returnModelAndView;

	}

	// @RequestMapping(value = "/processlogin", method = RequestMethod.POST)
	// public String login(@RequestParam("name") String usr,
	// @RequestParam("password") String password, Model model,
	// HttpServletResponse response, HttpServletRequest request) {
	//
	// System.out.println("DEBUG :: CALLED PROCESS LOGIN_CONTROLLER");
	//
	// LoginMsg msg = null;
	//
	// @SuppressWarnings("unchecked")
	// List<User> fetchedUsers = UserService.getInstance().getUserList();
	//
	// for (User user : fetchedUsers) {
	// if (checkPassword(password, user.getPassword())) {
	// if (user.getEmail().equals(usr)) {
	// // Found user for specified attributes
	//
	// HttpSession session = request.getSession();
	// session.setAttribute("user", user);
	// session.setMaxInactiveInterval(30 * 60);
	//
	// // Cookie cookie = new Cookie("user",
	// // String.valueOf(user.getUsrID()));
	// // cookie.setMaxAge(30*60);
	// // response.addCookie(cookie);
	//
	// msg = new LoginMsg(0, "Willkommen zurück " + user.getName());
	// model.addAttribute("loginmsg", msg);
	//
	// break;
	//
	// }
	// }
	// }
	//
	// if (msg == null) {
	// msg = new LoginMsg(1, "Passwort oder E-Mail Adresse falsch!");
	// }
	//
	// System.out.println("DEBUG :: LoginMsg=" + msg.getMsg() + " |
	// LoginStatus=" + msg.getStatus());
	// model.addAttribute("loginmsg", msg);
	//
	// return "userdashboard";
	// }

	public static boolean checkPassword(String password, String storedHash) {
		if (storedHash == null || !storedHash.startsWith("$2a$")) {
			return false;
		}
		return BCrypt.checkpw(password, storedHash);
	}

	public static String hashPassword(String password) {
		// salt workload
		int workload = 12;
		String salt = BCrypt.gensalt(workload);
		return BCrypt.hashpw(password, salt);
	}
}
