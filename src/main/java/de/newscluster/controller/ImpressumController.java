package de.newscluster.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ImpressumController {

	@RequestMapping("/impressum")
	public String impressum(Model model) {
		return "impressum";
	}
}
