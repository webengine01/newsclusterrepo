package de.newscluster.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import de.newscluster.controller.helper.FBShare;
import de.newscluster.controller.helper.Tag;
import de.newscluster.model.Article;
import de.newscluster.model.Comment;
import de.newscluster.service.ArticleService;
import de.newscluster.service.CommentService;
import de.newscluster.service.TopicService;

@Controller
public class DetailViewController {

	@RequestMapping("/detail_req={id}")
	public String showArticle(@PathVariable("id") int topicID, Model model) {
		ArrayList<Tag> tagList = new ArrayList<>();
		String[] split = TopicService.getInstance().retrieveByID(topicID).getTags().split(",");
		for (int i = 0; i < split.length; i++) {
			tagList.add(new Tag(split[i].trim()));
		}
		List<Article> artList = new ArrayList<>();
		artList = ArticleService.getInstance().retriveByTopic(topicID);
		int i = new Random().nextInt(((artList.size() - 1) - 0) + 1) + 0;
		if (i > artList.size() - 1) {
			i = artList.size() - 1;
		}
		model.addAttribute("mainArticle", artList.get(i));
		model.addAttribute("tagList", tagList);
		model.addAttribute("articles", artList);

		ArrayList<Comment> commentList = new ArrayList<>();
		commentList.addAll(CommentService.getInstance().retriveByID(topicID));
		model.addAttribute("fbshare", new FBShare("https://localhost:9888/detail_req=" + String.valueOf(topicID)));
		model.addAttribute("comments", commentList);
		return "detailview";
	}

}