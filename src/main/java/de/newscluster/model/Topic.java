package de.newscluster.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="topic", 
       uniqueConstraints={@UniqueConstraint(columnNames={"top_id"})})
public class Topic {
	
	@Id
	@GenericGenerator(name="inc" , strategy="increment")
	@GeneratedValue(generator="inc")
	@Column(name="top_id", length=11, nullable=false)
	private int topicID;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "rank_id")
	private Rank rank;
	
	@Column(name="topic", length=255, nullable=false)
	private String topic;
	
	@Column(name="tags", length=255, nullable=false)
	private String tags;
	
	public Topic(String topic, String tags) {
		super();
		this.topic = topic;
		this.tags = tags;
	}
	
	public Topic() {
		// TODO Auto-generated constructor stub
	}
	
	public int getTopicID() {
		return topicID;
	}
	
	public void setTopicID(int topicID) {
		this.topicID = topicID;
	}
	
	public String getTopic() {
		return topic;
	}


	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	public String getTags() {
		return tags;
	}
	
	public void setTags(String tags) {
		this.tags = tags;
	}
	
	public Rank getRank() {
		return rank;
	}

	public void setRank(Rank rank) {
		this.rank = rank;
	}

}
