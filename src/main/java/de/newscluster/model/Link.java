package de.newscluster.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="link", 
       uniqueConstraints={@UniqueConstraint(columnNames={"link_id"})})
public class Link {
	

	@Id
	@GenericGenerator(name="inc" , strategy="increment")
	@GeneratedValue(generator="inc")
	@Column(name="link_id", length=11, nullable=false)
	private int linkID;
	
	@Column(name="link", length=255, nullable=false)
	private String link;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "art_id")
	private Article artID;
	
	@Column(name="provider", length=255, nullable=false)
	private String provider;
	
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Link() {
	}

	public Link(String link) {
		this.link = link;
	}

	public int getLinkID() {
		return linkID;
	}

	public void setLinkID(int linkID) {
		this.linkID = linkID;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Article getArtID() {
		return artID;
	}

	public void setArtID(Article artID) {
		this.artID = artID;
	}
	
	
}
