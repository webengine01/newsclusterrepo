package de.newscluster.model.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class PasswordLostForm {

	@Size(min = 3, max = 30, message = "Min. 3 Zeichen, max. 30 Zeichen")
	@NotNull
	@Pattern(regexp = "[\\w\\s\\!\\?\\.,\\$\\&]{3,30}", message = "Darf nur die Zeichen a-z A-Z 0-9 !?.,$& enthalten!")
	private String name;
	
	@Size(min = 6, max = 300)
	@NotNull
	@Pattern(regexp = "[\\w!#\\$%&\\'\\+-/=\\?\\^_`\\{\\|}~]+\\.?([\\w!#\\$%&\\'\\+-/=\\?\\^_`\\{\\|}~]+)?@([\\w]+[\\w-]*\\.)?[\\w]+[\\w-]*[\\w]*\\.[\\w-]{2,3}", message = "E-Mail Adresse nicht valide!")
	private String email;

	public PasswordLostForm(String name, String email) {
		this.name = name;
		this.email = email;
	}
	
	public PasswordLostForm() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
