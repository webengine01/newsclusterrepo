package de.newscluster.model.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class LoginForm {

	@Size(min = 6, max = 300)
	@NotNull
	@Pattern(regexp = "[\\w!#\\$%&\\'\\+-/=\\?\\^_`\\{\\|}~]+\\.?([\\w!#\\$%&\\'\\+-/=\\?\\^_`\\{\\|}~]+)?@([\\w]+[\\w-]*\\.)?[\\w]+[\\w-]*[\\w]*\\.[\\w-]{2,3}", message = "E-Mail Adresse nicht valide!")
	private String email;

	@Size(min = 6, max = 20)
	@NotNull
	@Pattern(regexp = "[0-9a-fA-F\\S\\p{Punct}]{6,20}", message = "Min. 6 Zeichen, max. 20 Zeichen. Passwort darf nur 0-9, a-z, A-Z, enthalten!")
	private String password;

	public LoginForm() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String toString() {
		return "LoginForm [email=" + email + ", password=" + password + "]";
	}

}
