package de.newscluster.model.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RegisterForm {

	@Size(min = 3, max = 30, message = "Min. 3 Zeichen, max. 30 Zeichen")
	@NotNull
	@Pattern(regexp = "[\\w\\s\\!\\?\\.,\\$\\&]{3,30}", message = "Darf nur die Zeichen a-z A-Z 0-9 !?.,$& enthalten!")
	private String name;

	@Size(min = 6, max = 20)
	@NotNull
	@Pattern(regexp = "[0-9a-fA-F\\$\\#\\%\\&\\-\\_\\S\\p{Punct}]{6,20}", message = "Min. 6 Zeichen, max. 20 Zeichen. Passwort darf nur 0-9, a-z, A-Z, enthalten!")
	private String password;

	@Size(min = 6, max = 300)
	@NotNull
	@Pattern(regexp = "[\\w!#\\$%&\\'\\+-/=\\?\\^_`\\{\\|}~]+\\.?([\\w!#\\$%&\\'\\+-/=\\?\\^_`\\{\\|}~]+)?@([\\w]+[\\w-]*\\.)?[\\w]+[\\w-]*[\\w]*\\.[\\w-]{2,3}", message = "E-Mail Adresse nicht valide!")
	private String email;

	public RegisterForm() {
	}

	public RegisterForm(String name, String password, String email) {
		super();
		this.name = name;
		this.password = password;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return "RegisterForm [name=" + name + ", password=" + password + ", email=" + email + "]";
	}

}
