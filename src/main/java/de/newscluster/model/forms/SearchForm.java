package de.newscluster.model.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SearchForm {

	@NotNull
	@Size(min = 2, max = 30, message = "Min. 2 Zeichen, max. 200 Zeichen")
	@Pattern(regexp = "[_a-zA-Z0-9]{2,200}", message = "Darf nur die Zeichen a-z A-Z 0-9 enthalten!")
	private String content;

	
	public SearchForm() {
	}
	
	public SearchForm(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
