package de.newscluster.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "contact", uniqueConstraints = { @UniqueConstraint(columnNames = { "con_id" }) })
public class Contact {

	/*
	 * FOR VALIDATION:
	 * http://kielczewski.eu/2015/07/spring-recaptcha-v2-form-validation/
	 */

	@Id
	@GenericGenerator(name = "inc", strategy = "increment")
	@GeneratedValue(generator = "inc")
	@Column(name = "con_id", length = 11, nullable = false)
	private int reqID;
	

	@Column(name = "content", length = 255, nullable = false)
	private String content;

	@Column(name = "email", length = 255, nullable = false)
	private String email;

	@Column(name = "name")
	private String reqName;

	public Contact() {
	}

	public Contact(String content, String email, String reqName) {
		super();
		this.content = content;
		this.email = email;
		this.setReqName(reqName);
	}

	public int getReqID() {
		return reqID;
	}

	public void setReqID(int reqID) {
		this.reqID = reqID;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReqName() {
		return reqName;
	}

	public void setReqName(String reqName) {
		this.reqName = reqName;
	}

}
