package de.newscluster.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="rank", 
       uniqueConstraints={@UniqueConstraint(columnNames={"rank_id"})})
public class Rank {
	
	@Id
	@GenericGenerator(name="inc" , strategy="increment")
	@GeneratedValue(generator="inc")
	@Column(name="rank_id", length=11, nullable=false)
	private int rankID;
	
	@Column(name="score", nullable=false)
	private int rank;
	
	public Rank(int rank) {
		super();
		this.rank = rank;
	}
	public Rank() {
	}
	
	public int getRankID() {
		return rankID;
	}
	public void setRankID(int rankID) {
		this.rankID = rankID;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}

}
