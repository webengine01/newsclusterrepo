package de.newscluster.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "comment", uniqueConstraints = { @UniqueConstraint(columnNames = { "com_id" }) })
public class Comment {

	@Id
	@GenericGenerator(name = "inc", strategy = "increment")
	@GeneratedValue(generator = "inc")
	@Column(name = "com_id", length = 11, nullable = false)
	private int comID;

	@Column(name = "content", length = 255, nullable = false)
	private String content;

	@Column(name = "date", nullable = false)
	private Date date;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "topic_id")
	private Topic topic;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "usr_id")
	private User usr;

	public Comment() {

	}

	public Comment(String content, Topic topic, User usr) {
		super();
		this.content = content;
		this.topic = topic;
		this.usr = usr;
		this.date = new Date();
	}

	public int getComID() {
		return comID;
	}

	public void setComID(int comID) {
		this.comID = comID;
	}

	public Date getDate() {
		return date;
	}

	public String getDateString() {
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy : hh:mm:ss");
		return df.format(getDate());
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public User getUsr() {
		return usr;
	}

	public void setUsr(User usr) {
		this.usr = usr;
	}

}
