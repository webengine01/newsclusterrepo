package de.newscluster.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "article", uniqueConstraints = { @UniqueConstraint(columnNames = { "art_id" }) })
public class Article {

	@Id
	@GenericGenerator(name = "inc", strategy = "increment")
	@GeneratedValue(generator = "inc")
	@Column(name = "art_id", nullable = false, unique = true, length = 11)
	private int artID;

	@Column(name = "headline", length = 255, nullable = false)
	private String headLine;

	@Column(name = "content", length = 255, nullable = false)
	private String content;

	@Column(name = "date", nullable = false)
	private Date date;

	/*
	 * wäre besser wenn es eine Liste aus Tags wäre
	 */
	@Column(name = "tags", nullable = false)
	private String tags;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "rank_id")
	private Rank rankID;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "top_id")
	private Topic topicID;

	@OneToOne
	@JoinColumn(name = "art_id")
	private Link link;

	@OneToOne
	@JoinColumn(name = "art_id")
	private Image img;

	public Article(String headLine, String content, Date date) {
		this.headLine = headLine;
		this.content = content;
		this.date = date;
	}

	public Article() {
	}

	public int getArtID() {
		return artID;
	}

	public void setArtID(int artID) {
		this.artID = artID;
	}

	public String getHeadLine() {
		return headLine;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Rank getRankID() {
		return rankID;
	}

	public void setRankID(Rank rankID) {
		this.rankID = rankID;
	}

	public Topic getTopicID() {
		return topicID;
	}

	public void setTopicID(Topic topicID) {
		this.topicID = topicID;
	}

	public void setHeadLine(String headLine) {
		this.headLine = headLine;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}
	
	public String getDateString() {
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		return df.format(getDate());
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}

	public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		this.img = img;
	}
	
	public ArrayList<String> getTagList() {
		ArrayList<String> tagList = new ArrayList<>();
		String[] splitStrings = this.tags.split(",");
		
		for (String string : splitStrings) {
			string.trim();
			tagList.add(string);
		}
		
		return tagList;

	}

}
