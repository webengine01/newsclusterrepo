package de.newscluster.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="track", 
       uniqueConstraints={@UniqueConstraint(columnNames={"trk_id"})})
public class Track {
	
	@Id
	@GenericGenerator(name="inc" , strategy="increment")
	@GeneratedValue(generator="inc")
	@Column(name="trk_id", length=11, nullable=false)
	private int trkID;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="top_id")
	private Topic topicID;

	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="usr_id")
	private User usrID;
	
	
	public Track(Topic topicID, User usrID) {
		super();
		this.topicID = topicID;
		this.usrID = usrID;
	}
	
	public Track() {
		// TODO Auto-generated constructor stub
	}

	public int getTrkID() {
		return trkID;
	}
	
	public void setTrkID(int trkID) {
		this.trkID = trkID;
	}
	
	public Topic getTopicID() {
		return topicID;
	}
	
	public void setTopicID(Topic topicID) {
		this.topicID = topicID;
	}
	
	public User getUsrID() {
		return usrID;
	}
	
	public void setUsrID(User usrID) {
		this.usrID = usrID;
	}

}
