package de.newscluster.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "\"user\"", uniqueConstraints = { @UniqueConstraint(columnNames = { "usr_id" }) })
public class User {

	@Id
	@GenericGenerator(name = "inc", strategy = "increment")
	@GeneratedValue(generator = "inc")
	@Column(name = "usr_id", nullable = false, unique = true, length = 11)
	private int usrID;

	@Column(name = "name", nullable = false, unique = true, length = 255)
	private String name;

	@Column(name = "password", nullable = false, unique = true, length = 50)
	private String password;

	@Column(name = "email", nullable = false, unique = true, length = 50)
	private String email;

//	@OneToMany(cascade = CascadeType.ALL)
//	@JoinColumn(name = "role_id")
//	private Collection<Role> roleID;

	@Transient
	private boolean authenticated;

	public User(String name, String password, String email) {
		super();
		this.name = name;
		this.password = password;
		this.email = email;
	}

	public User() {
	}

	public int getUsrID() {
		return usrID;
	}

	public void setUsrID(int usrID) {
		this.usrID = usrID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAuthentication(boolean authenticated) {
		this.authenticated = authenticated;
	}
	
	public boolean isAuthenticated(){
		return authenticated;
	}

}
