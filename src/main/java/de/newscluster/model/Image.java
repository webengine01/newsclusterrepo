package de.newscluster.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="img", 
       uniqueConstraints={@UniqueConstraint(columnNames={"img_id"})})
public class Image {
	
	@Id
	@GenericGenerator(name="inc" , strategy="increment")
	@GeneratedValue(generator="inc")
	@Column(name="img_id", length=11, nullable=false)
	private int imgID;
	
	@Column(name="path", length=255, nullable=false)
	private String path;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="art_id")
	private Article artID;
	
	public Image(String path) {
		this.path = path;
	}
	
	public Image() {
		// TODO Auto-generated constructor stub
	}
	
	
	public int getImgID() {
		return imgID;
	}
	public void setImgID(int imgID) {
		this.imgID = imgID;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Article getArtID() {
		return artID;
	}
	public void setArtID(Article artID) {
		this.artID = artID;
	}

}
