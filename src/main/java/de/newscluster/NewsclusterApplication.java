package de.newscluster;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class NewsclusterApplication {

	public static void main(String[] args) throws IOException {

		SpringApplication.run(NewsclusterApplication.class, args);
	}
}
