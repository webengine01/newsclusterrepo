(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


hashCode = function(s) {
	return s.split("").reduce(function(a, b) {
		a = ((a << 5) - a) + b.charCodeAt(0);
		return a & a
	}, 0);
}

function getBaseUrl() {
	var re = new RegExp(/^.*\//);
	return re.exec(window.location.href);
}


$(function() {
	$('#secondFold').css({
		top : $(window).height()
	});
});

// set the format for the bootstrap datepicker
// (is imported over CDN)
// ================================================

$(function(){
	$('#datepicker').datepicker({
		format: 'dd.mm.yyyy', language: 'de'
	});
});


// show the loading message while the ajax call is
// loading
// ================================================

function styleLoginModalWhileLoading(isLoading) {
	
	if (isLoading) {
		$('#loginModalBody').addClass('disabledBox');
		$('#loginModalFooter').addClass('disabledBox');
		$('#loginLoadingMessage').show();  // show the loading message.
	} else {
		$('#loginLoadingMessage').hide(); // hide the loading message
		$('#loginModalBody').removeClass('disabledBox');
		$('#loginModalFooter').removeClass('disabledBox');
	}
}



// collapse the navbar when scrolling
// ================================================

function collapseNavbar() {
	if ($(".navbar").offset().top > 50) {
		$(".navbar-fixed-top").addClass("top-nav-collapse");
	} else {
		$(".navbar-fixed-top").removeClass("top-nav-collapse");
	}
}
$(window).scroll(collapseNavbar);
$(document).ready(collapseNavbar);

// show the fancy popover in the detailview
// =================================================

$(document).ready(function(){
 $('[data-toggle="popover"]').popover(); 
});


// COOKIE CHECK
// ================================================
// if js is enabled, we have to check if Cookies also enabled.
// true -> disable noScriptBanner
// false -> show noScriptBanner for cookies
// ================================================


$(document).ready(function() {	
	checkCookie();
});

function checkCookie(){
    var cookieEnabled=(navigator.cookieEnabled)? true : false;
   
    if (typeof navigator.cookieEnabled=="undefined" && !cookieEnabled){ 
        document.cookie="testcookie";
        cookieEnabled=(document.cookie.indexOf("testcookie")!=-1)? true : false;
    }
    
    if (!cookieEnabled) {
    	console.log("COOKIES ARE DISABLED");
        $('#noScriptBanner_cookies').show();
    }  
}


// COMMENT FORM VALIDATION
// calls manually the html5 validator from the comment form
// ================================================

 function submitCommentForm() {
 var f = document.getElementById("commentform");
 if (f.checkValidity()) {
 sendNewComment();
 } else {
 $("commentform").validator('validate');
 }
 }

 $(document).ready(function() {
 function changeCommitButton() {

 var f = document.getElementById("commentform");

 if (f.checkValidity() == false) {
 document.getElementById("submitBtn").className += "disabled";
 }
 }
 });



// CREATE DYNAMIC LINKS
// ================================================
// create the dynamic links for the similiar topic
// search, the archive filter and the deletefeed query
// ===============================================

$(document).ready(function() {
	$('#simTopics').click(function() {
		var url = getBaseUrl();
		var tagsArray = document.getElementsByClassName("tagElement");
		var tagString = "";
		var tagLength = tagsArray.length;
		for (var i = 0; i < tagLength; i++) {
		    tagString += tagsArray[i].innerHTML + "&";
		}
		var newParam = "search=" + tagString;
		url += newParam;
		window.location.href = url;
		return false;
	})
});

function deleteFeed(topicName, userName) {
	// var topicElement = $("topicElement&" + topicName);
	// topicElement.hide();
		
		var url = getBaseUrl();
		var topicID = window.location.href.split('=')[1];
		var url = url + "deletefeed=" + topicName; 
		window.location.href = url;
		return false;
	}


function followTopic() {
		var url = getBaseUrl();
		var topicID = window.location.href.split('=')[1];
		var url = url + "follow=" + topicID; 
		window.location.href = url;
		return false;
}

$(document).ready(function() {
	$('#filterButton').click(
					function() {
						var url = getBaseUrl();
						var newParam = "archive="
								+ $('#tagSearch').val() + "from="
								+ $('#archDate0').val() + "to="
								+ $('#archDate1').val() + "&";
						url += newParam;
						url = encodeURI(url);
						window.location.href = url;
						return false;
					})
});

// BUTTON LISTENER
// ================================================

// The most ajax calls are for validation (if validation not performed
// bean-backed form from thymeleaf)
// =================================================

function pressSearchEnter() {
	var searchText = $('#searchFieldInput');
	var searchButton = $('#searchFieldButton');
	
	searchText.addEventListener("keypress", function(event) {
		if (event.keyCode == 13) {
			searchButton.click();
		}
	}
	)
}

function pressLoginEnter() {
	var loginMail = $('#signMailField');
	var loginPassword = $('#signPwField');
	var loginButton = $('#signInButton');
	
	loginMail.addEventListener("keypress", function(event) {
		if (event.keyCode == 13) {
			loginButton.click();
		}
	});
		loginPassword.addEventListener("keypress", function(event) {
		if (event.keyCode == 13) {
			loginButton.click();
		}
	});
}

function openLogin() {
	$(document).ready(function() {
		// show Modal
		$('#loginModal').modal('show');
		}
	);
}

$(document).ready(function() {
	// Add smooth scrolling to all links
	$("#scrollCircle").on('click', function(event) {
		var hash = "#secondFold";
		// Make sure this.hash has a value before overriding default behavior
		if (this.hash !== "") {
			event.preventDefault();
			var hash = this.hash;
			$('html, body').animate({
				scrollTop : $(hash).offset().top
			}, 800, function() {
				window.location.hash = hash;
			});
		} // End if
	});
});

// AJAX
// ================================================

// The most ajax calls are for validation (if validation not performed
// bean-backed form from thymeleaf)
// =================================================

function submitSearch() {
	var contentValue = $('#searchFieldInput').val();
	console.log("SEARCH QUERY : " + contentValue);

	// perform an asynchronous HTTP (Ajax) request.
	// https://api.jquery.com/jquery.ajax/
	
	$.ajax({
				type : "POST",
				url : "/validatesearch",
				data : "content=" + contentValue,
				dataType : "text json",
				success : function(data, textStatus, jqXHR) {
													
					if (data.status == "SUCCESS") {
						window.location.href = "/search=" + contentValue;
					}
					
					hasContentError = false;
					contentErrorMessage = '<div class="help-block with-errors"><ul class="list-unstyled">';
				
					for (singleError of data.errorMessageList) {
						if (singleError.field == 'content') {
							console.log("CONTENT-ERROR");
							contentErrorMessage += '<li>' + singleError.defaultMessage + '</li>';
							hasContentError = true;
						} else {
							console.log("UNKNOWN-ERROR :" + singeleError.field);
						}
					}
					
					if (hasContentError) {
						$('#searchBoxFormGroup')[0].className += 'has-danger has-error '; 
						$('#searchBoxInputAddon')[0].className += 'glyphicon-remove ';
						
						contentErrorMessage += '</ul></div>';
						$('#errorFieldBackground').removeAttr('hidden');
						$('#searchBoxErrorMessage').html(contentErrorMessage);
					}									
				},

				
				// if the controller cant be reached
				error : function(jqXhr) {

					var errors = jqXhr.xresponseJSON; // this will get the
					// errors
					errorsHtml = '<div class="alert alert-danger"><p>Login zur Zeit möglich ['
							+ errors.status + ']</p>';

					$('#searchBarErrorMessage').html(errorsHtml);

				},

				done : function(e) {
					console.log("DONE");
				}
			});
}

/* ######################################################################### */


function sendNewComment() {

	// get the form values
	var emailValue = $('#commentEmailField').val();
	var nameValue = $('#commentNameField').val();
	var contentValue = $('#commentTextArea').val();
	
	$.ajax({
				type : "POST",
				url : "/newcomment",
				data : "name="+ nameValue + "&email=" + emailValue + "&content=" + contentValue,
				dataType : "text json",
				success : function(data, textStatus, jqXHR) {
														
					if (data.status == "SUCCESS") {
						// hide the commentform
						$('#collapseCommentForm').click();
						getNewestComment();
					}
					
					hasEmailError = false;
					hasNameError = false;
					hasContentError = false;
					hasUnknownError = false;
					
					emailErrorMessage = '<div class="help-block with-errors"><ul class="list-unstyled">';
					nameErrorMessage = '<div class="help-block with-errors"><ul class="list-unstyled">';
					contentErrorMessage = '<div class="help-block with-errors"><ul class="list-unstyled">';

					unknownErrorMessage = '<div class="alert alert-danger"><ul>';
					
					for (singleError of data.errorMessageList) {
						
						if (singleError.field == 'email') {
							emailErrorMessage += '<li>' + singleError.defaultMessage + '</li>';
							hasEmailError = true;
						} else if (singleError.field == 'name') {
							nameErrorMessage += '<li>' + singleError.defaultMessage + '</li>';
							hasNameError = true;
						} else if (singleError.field == 'content') {
							contentErrorMessage += '<li>' + singleError.defaultMessage + '</li>';
							hasContentError = true;
						} else {
							unknownErrorMessage += '<li>' + singleError.defaultMessage + '</li>';
							hasUnknownError = true;	
						}
					}

					if (hasEmailError) {
						$('#commentEmailFormGroup')[0].className += 'has-danger has-error '; 
						$('#commentEmailInputAddon')[0].className += 'glyphicon-remove';
						
						emailErrorMessage += '</ul></div>';
						$('#loginEmailErrorMessage').html(emailErrorMessage);
					}

					if (hasNameError) {
						$('#commentNameFormGroup')[0].className += 'has-danger has-error '; 
						$('#commentNameInputAddon')[0].className += 'glyphicon-remove';
						
						passwordErrorMessage += '</ul></div>';
						$('#commentNameErrorMessage').html(nameErrorMessage);
					}
					
					if (hasContentError) {
						$('#commentContentFormGroup')[0].className += 'has-danger has-error '; 
						$('#commentContentInputAddon')[0].className += 'glyphicon-remove';
						
						contentErrorMessage += '</ul></div>';
						$('#commmentContentErrorMessage').html(contentErrorMessage);
					}

					if (hasUnknownError) {
						unknownErrorMessage += '</ul></div>';
						$('#loginErrorMessage').html(unknownErrorMessage);
					}										
				},

				// if the controller cant be reached
				error : function(jqXhr) {
					var errors = jqXhr.xresponseJSON; // this will get the
														// errors
					errorsHtml = '<div class="alert alert-danger"><p>Kein Login zur Zeit möglich ['
							+ errors.status + ']</p>';
					$('#loginErrorMessage').html(errorsHtml);
				}
				
			});
}

/* ######################################################################### */

function postLoginForm() {

	// get the form values
	var emailValue = $('#signMailField').val();
	var passwordValue = $('#signPwField').val();

	styleLoginModalWhileLoading(true);
	
	// perform an asynchronous HTTP (Ajax) request.
	// https://api.jquery.com/jquery.ajax/
	$.ajax({
				type : "POST",
				url : "/login",
				data : "email=" + emailValue + "&password=" + passwordValue,
				dataType : "text json",
				success : function(data, textStatus, jqXHR) {
									
					styleLoginModalWhileLoading(false);
					
					// if succes, then the validated data will be sent to the
					// real processLogin-Controller, to
					// check the name/password
					
					if (data.status == "SUCCESS") {
						window.location.href = "/userdashboard";
					}
										
					hasEmailError = false;
					hasPasswordError = false;
					hasUnknownError = false;
					
					emailErrorMessage = '<div class="help-block with-errors"><ul class="list-unstyled">';
					passwordErrorMessage = '<div class="help-block with-errors"><ul class="list-unstyled">';
					unknownErrorMessage = '<div class="alert alert-danger"><ul>';
					
					for (singleError of data.errorMessageList) {
						
						if (singleError.field == 'email') {
							emailErrorMessage += '<li>' + singleError.defaultMessage + '</li>';
							hasEmailError = true;
						}
						else if (singleError.field == 'password') {
							passwordErrorMessage += '<li>' + singleError.defaultMessage + '</li>';
							hasPasswordError = true;
						} else {
							unknownErrorMessage += '<li>' + singleError.defaultMessage + '</li>';
							hasUnknownError = true;	
						}
					}
					
					if (hasEmailError) {
						$('#loginEmailFormGroup')[0].className += 'has-danger has-error'; 
						$('#loginEmailInputAddon')[0].className += 'glyphicon-remove';
						
						emailErrorMessage += '</ul></div>';
						$('#loginEmailErrorMessage').html(emailErrorMessage);
					}
					if (hasPasswordError) {
						$('#loginPasswordFormGroup')[0].className += 'has-danger has-error'; 
						$('#loginPasswordInputAddon')[0].className += 'glyphicon-remove';
						
						passwordErrorMessage += '</ul></div>';
						$('#loginPasswordErrorMessage').html(passwordErrorMessage);
					}
					if (hasUnknownError) {

						unknownErrorMessage += '</ul></div>';
						$('#loginErrorMessage').html(unknownErrorMessage);
					}										
				},

				// if the controller cant be reached
				error : function(jqXhr) {

					var errors = jqXhr.xresponseJSON; // this will get the
					// errors
					errorsHtml = '<div class="alert alert-danger"><p>Login zur Zeit nicht möglich ['
							+ errors.status + ']</p>';
					$('#loginErrorMessage').html(errorsHtml);
				}
			});
	return false;
}

// load the newest comment from topic (the comment the user write right now)
// ========================================================================

 function getNewestComment() {

	 var artId = $('#topicIdField').val();
	 var url = "/comments/" + artId + "&";
	 var seperator = "=";
	 var nameParam = "name" + seperator + $('#commentNameField').val();
	 var emailParam = "email" + seperator + $('#commentEmailField').val();
	 var contentParam = "content" + seperator + $('#commentTextArea').val();
		
	 url = url + nameParam + "&" + emailParam + "&" + contentParam;
	 url = encodeURI(url);
	
	 $("#oldComments").load(url);
	 $("#commentform")[0].reset();

	 // einfügen des neuen Kommentars
	 $.get(url, function(data) {
		 $("#placeholderNewComment").replaceWith(data);
	 	}
	 );
 }
