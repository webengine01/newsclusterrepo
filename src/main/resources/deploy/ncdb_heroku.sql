CREATE TABLE public.rank
(
  rank_id integer NOT NULL,
  score integer,
  CONSTRAINT pk_rank PRIMARY KEY (rank_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.rank
  OWNER TO pldyglpbhfsydo;
  
  
  
  
 CREATE TABLE public.topic
(
  top_id integer NOT NULL,
  tags text,
  topic text,
  rank_id integer,
  CONSTRAINT pk_top PRIMARY KEY (top_id),
  CONSTRAINT fk_top FOREIGN KEY (rank_id)
      REFERENCES public.rank (rank_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.topic
  OWNER TO pldyglpbhfsydo;

  
  CREATE TABLE public.article
(
  art_id integer NOT NULL,
  headline text,
  content text,
  date date,
  tags text,
  rank_id integer,
  top_id integer,
  CONSTRAINT pk_art PRIMARY KEY (art_id),
  CONSTRAINT fk1_article FOREIGN KEY (rank_id)
      REFERENCES public.rank (rank_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk2_article FOREIGN KEY (top_id)
      REFERENCES public.topic (top_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.article
  OWNER TO pldyglpbhfsydo;
  
CREATE TABLE public."user"
(
  usr_id integer NOT NULL,
  name text,
  password text,
  email text,
  CONSTRAINT pk_usr PRIMARY KEY (usr_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."user"
  OWNER TO pldyglpbhfsydo;

CREATE TABLE public.comment
(
  com_id integer NOT NULL,
  content text,
  topic_id integer,
  usr_id integer,
  date date,
  CONSTRAINT pk_com PRIMARY KEY (com_id),
  CONSTRAINT fk1_com FOREIGN KEY (topic_id)
      REFERENCES public.topic (top_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk2_com FOREIGN KEY (usr_id)
      REFERENCES public."user" (usr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.comment
  OWNER TO pldyglpbhfsydo;
  
  
  
 CREATE TABLE public.img
(
  img_id integer NOT NULL,
  path text,
  art_id integer,
  CONSTRAINT pk_img PRIMARY KEY (img_id),
  CONSTRAINT fk_img FOREIGN KEY (art_id)
      REFERENCES public.article (art_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.img
  OWNER TO pldyglpbhfsydo;




 CREATE TABLE public.track
(
  trk_id integer NOT NULL,
  top_id integer,
  usr_id integer,
  CONSTRAINT pk_trk PRIMARY KEY (trk_id),
  CONSTRAINT fk1_track FOREIGN KEY (top_id)
      REFERENCES public.topic (top_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk2_trk FOREIGN KEY (usr_id)
      REFERENCES public."user" (usr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.track
  OWNER TO pldyglpbhfsydo;
 
  
  
  
CREATE TABLE public."link"
(
  link_id integer NOT NULL,
  link text,
  art_id integer,
  provider text,
  CONSTRAINT pk_link PRIMARY KEY (link_id),
  CONSTRAINT fk_link FOREIGN KEY (art_id)
      REFERENCES public.article (art_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public."link"
  OWNER TO pldyglpbhfsydo;

CREATE TABLE public.contact
(
  con_id integer NOT NULL,
  content text,
  email text,
  name text,
  CONSTRAINT pk_contact PRIMARY KEY (con_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.contact
  OWNER TO pldyglpbhfsydo;

