insert into rank (rank_id, score) values (1, 1);

insert into topic (top_id, tags, topic, rank_id) values (1, 'Die Linke, IS, Klage, Bundeswehreinsatz', 'Verfassungsklage gegen Bundeswehreinsatz im Irak und Syrien',1);

insert into article (art_id, headline, content, date, tags, rank_id, top_id) values (1,'"Islamischer Staat": Linke klagt gegen deutschen Anti-IS-Kampf','Die Linke hat Verfassungsklage gegen den Bundeswehreinsatz im Irak und Syrien eingelegt. Dem Kampfeinsatz gegen den "Islamischen Staat" fehle das Mandat.','31.05.2016','Die Linke, IS, Klage, Bundeswehreinsatz',1,1);
insert into link (link_id, link, art_id, provider) values (1,'http://www.zeit.de/politik/deutschland/2016-05/islamischer-staat-bundestag-die-linke-verfassungsklage',1,'zeit.de');
insert into img (img_id, path, art_id) values (1, 'http://img.zeit.de/politik/deutschland/2016-05/die-linke-bundestag-verfassungsklage-deutscher-militaereinsatz-is/bitblt-820x461-a3e51d71c8dda868e84c15b7e77b675bec3f935b/wide', 1);

insert into article (art_id, headline, content, date, tags, rank_id, top_id) values (2,'Linke klagt gegen Anti-IS-Kampf','Deutschland beteiligt sich am internationalen Kampf gegen den IS - mit Waffen, der Ausbildung von kurdischen Peschmerga und Aufklärungsflügen. Dieses Engagement sei nicht rechtens, sagt die Linke und zieht vor das Bundesverfassungsgericht.','31.05.2016','Die Linke, IS, Klage, Bundeswehreinsatz',1,1);
insert into link (link_id, link, art_id, provider) values (2,'http://www.n-tv.de/politik/Linke-klagt-gegen-Anti-IS-Kampf-article17823911.html',2,'n-tv.de');
insert into img (img_id, path, art_id) values (2, 'http://bilder1.n-tv.de/img/incoming/crop17824216/4444994385-cImg_16_9-w680/64810526.jpg', 2);

insert into article (art_id, headline, content, date, tags, rank_id, top_id) values (3,'Linke erhebt Verfassungsklage gegen deutschen Anti-IS-Kampf','Die Linke im Bundestag hat Verfassungsklage gegen den deutschen Militäreinsatz gegen die Terrormiliz Islamischer Staat (IS) eingelegt. Die Klage sei am Dienstag in Karlsruhe eingebracht worden, sagte ein Fraktionssprecher in Berlin.','31.05.2016','Die Linke, IS, Klage, Bundeswehreinsatz',1,1);
insert into link (link_id, link, art_id, provider) values (3,'http://www.schwaebische.de/politik/inland_artikel,-Linke-erhebt-Verfassungsklage-gegen-deutschen-Anti-IS-Kampf-_arid,10461396.html',3,'schwaebische.de');
insert into img (img_id, path, art_id) values (3, 'http://www.schwaebische.de/cms_media/module_img/5912/2956436_1_article660x420_der-fraktionsvorsitzende-der-partei-die-linke-im-bundestag-dietmar-bartsch-foto-peter-endig-archiv.jpg', 3);

/* BISHER KANN MAN KEINE ARTIKEL OHNE BILDER EINFÜGEN
 * 
 * insert into article (art_id, headline, content, date, tags, rank_id, top_id) values (4,'Linke erhebt Verfassungsklage gegen deutschen Anti-IS-Kampf','Die Linke im Bundestag hat Verfassungsklage gegen den deutschen Militäreinsatz gegen die Terrormiliz Islamischer Staat (IS) eingelegt. Die Klage sei am Dienstag in Karlsruhe eingebracht worden, sagte ein Fraktionssprecher in Berlin.','31.05.2016','Die Linke, IS, Klage, Bundeswehreinsatz',1,1);
 * insert into link (link_id, link, art_id, provider) values (4,'http://www.focus.de/politik/deutschland/terrorismus-linke-erhebt-verfassungsklage-gegen-deutschen-anti-is-kampf_id_5582126.html',1,'focus.de');
 */

insert into rank values (2, 2);
insert into rank values (3, 3);

insert into topic (top_id, tags, topic, rank_id) values (5, 'Atomkraft, EU', 'EU',2);
insert into topic values (2, 'Google, Technik', 'Technik',2);
insert into topic values (3, 'Flüchtlinge, Mittelmeer', 'Flüchtlinge',3);
insert into topic values (4, 'Böhmermann, Erdogan','Böhmermann',3);

insert into article (art_id, headline, content, date, tags, rank_id, top_id) values (6,'Atomkraft: EU-Kommission will Kernenergie in Europa stärken','Die EU-Kommission will nach SPIEGEL-ONLINE-Informationen den Bau von Atommeilern vorantreiben. Außerdem sollen neue Mini-Reaktoren entwickelt werden. Insider vermuten hinter den Plänen zwei Motive. In Deutschland soll 2022 das letzte ...','17.05.2016','Atomkraft, EU',1,2);
insert into article values (5,'Umwelt: UN-Experten: Glyphosat doch nicht krebserregend','Das umstrittene Pflanzenschutzmittel Glyphosat ist nach Einschätzung von UN-Experten wahrscheinlich nicht krebserregend. Dies geht aus einem am Montag in Genf vorgestellten Bericht hervor, der unter anderem von Experten ......','17.05.2016','EU, Glyphosat',1,2);
insert into article values (7,'Chrome sperrt künftig standardmäßig alle Flash-Inhalte','Google will offenbar das in Chrome integrierte Flash-Plug-in künftig standardmäßig deaktivieren. Nutzer, die Websites mit Inhalten besuchen, für die Adobes Flash Player benötigt wird, müssten dann das Plug-in zuerst aktivieren. Einer Präsentation .........','17.05.2016','Google, Technik',2,2);
insert into article values (8,'Mittelmeer: Mehr als 1100 Flüchtlinge gerettet','Deutschland erreichen immer weniger Flüchtlinge - doch über das Mittelmeer versuchen Tausende Menschen weiter nach Europa zu kommen. Allein am Montag wurden 1100 Migranten in Sicherheit gebracht. Noch immer wagen Tausende Menschen die ............','17.05.2016','Flüchtlinge, Italien',2,3);
insert into article values (9,'Gericht verbietet einzelne Passagen des "Schmähgedichts"','Â Erdogan gegen BÃ¶hmermann : Gericht verbietet einzelne Passagen des "Schmähgedichts". Erster Erfolg für den türkischen Staatspräsidenten: Jan Böhmermann darf einzelne Verse seines Erdogan-Gedichts nicht wiederholen, ... ............','17.05.2016','Böhmermann, Erdogan',2,4);

insert into link (link_id, link, art_id, provider) values (6,'http://www.spiegel.de/wirtschaft/unternehmen/atomkraft-eu-kommission-will-kernenergie-in-europa-staerken-a-1092584.html',6,'spiegel.de');
insert into link values (5,'http://www.welt.de/newsticker/news1/article155398515/UN-Experten-Glyphosat-doch-nicht-krebserregend.html',5,'welt.de');
insert into link values (7,'http://www.zdnet.de/88269323/chrome-sperrt-kuenftig-standardmaessig-alle-flash-inhalte/',7,'zdnet.de');
insert into link values (8,'http://www.spiegel.de/politik/ausland/mittelmeer-mehr-als-1100-fluechtlinge-gerettet-a-1092603.html',8,'spiegel.de');
insert into link values (9,'http://www.tagesspiegel.de/medien/erdogan-gegen-boehmermann-gericht-verbietet-einzelne-passagen-des-schmaehgedichts/13606714.html',9,'tagesspiegel.de');

insert into img (img_id, path, art_id) values (6, 'http://cdn4.spiegel.de/images/image-983807-breitwandaufmacher-pdwb-983807.jpg', 5);
insert into img values (4, 'http://img.welt.de/img/news1/crop153071814/6899738756-ci3x2l-w540/Glyphosat-ist-das-weltweit-am-meisten-verkaufte-Herbizid.jpg', 6);
insert into img values (5, 'https://t2.gstatic.com/images?q=tbn:ANd9GcT4IPGbOY6By_nxUuvEK-iETrct4DK9ibspZFCCvTUStb-Cw1i0_okwfF4uerJMa4qzfOVja2Wq', 7);
insert into img values (7, 'http://cdn2.spiegel.de/images/image-983501-breitwandaufmacher-qqop-983501.jpg', 8);
insert into img values (8, 'http://www.tagesspiegel.de/images/recep-tayyip-erdogan-jan-boehmermann/13606802/1-format6001.jpg', 9);

insert into "user" (usr_id, name, password, email) values(1, 'Anonymous', 'blablabla', 'hans@wurst.de');
insert into "user" values (2, 'Willi', 'blablabla', 'trash@mail.de');
insert into "user" values (3, 'Stephen', 'blablabla', 'ich@gmx.de');
insert into "user" values (4, 'Jüüüürgen', 'blablabla', 'Jürgen@example.de');

insert into comment (com_id, content, topic_id, usr_id, date) values (1, 'rischtisch geile sache hier', 1, 1,'2016-6-16 17:27:34.754000 +2:0:0');
insert into comment values (2, 'das ist hier definitiv falsch dargestellt!', 2, 2, '2016-6-18 12:48:34.754000 +2:0:0');
insert into comment values (3, 'lügenpresse! lügenpresse!', 3, 3, '2016-6-19 15:27:34.754000 +2:0:0');
insert into comment values (4, 'immer das gleiche auf diesen newsportalen', 2, 4, '2016-6-19 15:28:34.754000 +2:0:0');



